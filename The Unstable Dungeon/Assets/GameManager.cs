﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;
    public Floor Floor;

    public Canvas MapCanvas;
    public Canvas MiniMapCanvas;

    public Camera MapCamera;
    public Camera MinimapCamera;

    bool IsMapOpen;

    // Start is called before the first frame update
    void Start()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }

        CreateFloor();
    }

    public void OpenMap(InputAction.CallbackContext value)
    {
        if(value.started)
        {
            if(!IsMapOpen)
            {
                MapCanvas.gameObject.SetActive(true);
                MiniMapCanvas.gameObject.SetActive(false);
                IsMapOpen = true;
            }
            else
            {
                MapCanvas.gameObject.SetActive(false);
                MiniMapCanvas.gameObject.SetActive(true);
                IsMapOpen = false;
            }
        }

        SnapToCurrentRoom();
    }

    [ContextMenu("Create Floor")]
    public void CreateFloor()
    {
        Floor.CreateFloor();
    }

    public void SnapToCurrentRoom()
    {
        if (Floor.CurrentRoomCoordinates != null)
        {
            if (MapCamera != null)
            {
                MapCamera.transform.position = new Vector3(Floor.CurrentRoomCoordinates.x, Floor.CurrentRoomCoordinates.y, -40);
            }

            if (MinimapCamera != null)
            {
                MinimapCamera.transform.position = new Vector3(Floor.CurrentRoomCoordinates.x, Floor.CurrentRoomCoordinates.y, -40);
            }
        }
    }

    private void OnEnable()
    {
        Floor.OnCreateFloor += Floor_OnCreateFloor;
    }

    private void Floor_OnCreateFloor()
    {
        SnapToCurrentRoom();
    }

    private void OnDisable()
    {
        Floor.OnCreateFloor -= Floor_OnCreateFloor;
    }
}
