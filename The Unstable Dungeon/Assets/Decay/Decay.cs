﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Decay : MonoBehaviour
{
    public static Decay instance;
    public float MaxDecay;
    float currentDecay;

    public float DecayHealUponRoomCompletion;

    Image image;

    // Start is called before the first frame update
    void Start()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }

        image = GetComponent<Image>();
        currentDecay = MaxDecay;
    }

    // Update is called once per frame
    void Update()
    {
        if (currentDecay > 0)
        {
            currentDecay -= Time.deltaTime;
        }
        else
        {
            currentDecay = MaxDecay;
            GameManager.instance.Floor.DestroyRandomRoom();
        }

        image.fillAmount = currentDecay / MaxDecay;

    }

    public void HealDecay()
    {
        currentDecay += DecayHealUponRoomCompletion;

        if(currentDecay > MaxDecay)
        {
            currentDecay = MaxDecay;
        }
    }
}
