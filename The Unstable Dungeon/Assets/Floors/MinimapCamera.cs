﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MinimapCamera : MonoBehaviour
{
    public static MinimapCamera instance;

    // Start is called before the first frame update
    void Start()
    {
        if(instance == null)
        {
            instance = this;
        }
        else
        {
            DontDestroyOnLoad(gameObject);
        }
    }

    // Update is called once per frame
    void Update()
    {
    
    }

    public void SnapToCurrentRoom()
    {
        if (GameManager.instance.Floor.CurrentRoomCoordinates != null)
        {
            transform.position = new Vector3(GameManager.instance.Floor.CurrentRoomCoordinates.x, GameManager.instance.Floor.CurrentRoomCoordinates.y, -40);
        }
    }

    private void OnEnable()
    {
        Floor.OnCreateFloor += Floor_OnCreateFloor;
    }

    private void Floor_OnCreateFloor()
    {
        SnapToCurrentRoom();
    }

    private void OnDisable()
    {
        Floor.OnCreateFloor -= Floor_OnCreateFloor;
    }
}
