using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum RoomType
{
    RegularRoom,
    NoRoom,
    StartingRoom,
    TreasureRoom,
    Shop,
    BossRoom,
    NPCRoom
}

public enum Faction
{
    Black,
    Blue,
    Red,
    Yellow,
    Purple
}

[CreateAssetMenu(fileName = "Room", menuName = "Room", order = 1)]
public class Room : ScriptableObject
{
    public GameObject RoomPrefab;
    public GameObject NorthDoorPrefab;
    public GameObject EastDoorPrefab;
    public GameObject SouthDoorPrefab;
    public GameObject WestDoorPrefab;

    public GameObject RedPrefab;
    public GameObject BluePrefab;
    public GameObject YellowPrefab;
    public GameObject BlackPrefab;
    public GameObject PurplePrefab;
    public GameObject StartPrefab;
    public GameObject TreasurePrefab;
    public GameObject ShopPrefab;
    public GameObject BossPrefab;
    public GameObject NPCPrefab;

    public Dictionary<RoomType, bool> IsSpecialRoom = new Dictionary<RoomType, bool>();
    public RoomType RoomType;
    public Dictionary<Faction, float> FactionPower = new Dictionary<Faction, float>();
    public Faction Faction;
    public float NeutralThreshHold;

    public bool HasBeenCompleted;

    public GameObject EnemyPrefab;
    public GameObject EnemyBossPrefab;
    public int NumberOfEnemies;

    public void Initialise()
    {
        RoomType = RoomType.NoRoom;
        Faction = Faction.Black;

        IsSpecialRoom.Clear();
        IsSpecialRoom.Add(RoomType.RegularRoom, false);
        IsSpecialRoom.Add(RoomType.NoRoom, false);
        IsSpecialRoom.Add(RoomType.StartingRoom, true);
        IsSpecialRoom.Add(RoomType.TreasureRoom, true);
        IsSpecialRoom.Add(RoomType.Shop, true);
        IsSpecialRoom.Add(RoomType.BossRoom, true);
        IsSpecialRoom.Add(RoomType.NPCRoom, true);

        FactionPower.Clear();
        FactionPower.Add(Faction.Red, 0);
        FactionPower.Add(Faction.Yellow, 0);
        FactionPower.Add(Faction.Black, 0);
        FactionPower.Add(Faction.Blue, 0);
        FactionPower.Add(Faction.Purple, 0);

        HasBeenCompleted = false;
    }

    public void DeclareFaction()
    {
        float HighestPower = 0;

        for (int i = 0; i < FactionPower.Count; i++)
        {
            if ((FactionPower[(Faction)i] > HighestPower))
            {
                HighestPower = FactionPower[(Faction)i];
                Faction = (Faction)i;
            }
        }
    }

    public void RandomiseNumberOfEnemies()
    {
        NumberOfEnemies = Random.Range(0, 4);
    }

    public void SpawnEnemies()
    {
        for (int i = 0; i < NumberOfEnemies; i++)
        {
            Instantiate(EnemyPrefab, new Vector3(Random.Range(-3, 3), 2, Random.Range(-3,3)), Quaternion.Euler(0,0,0), GameManager.instance.Floor.currentRoomPrefab.transform);
        }

        if(NumberOfEnemies == 0)
        {
            HasBeenCompleted = true;
            Decay.instance.HealDecay();
        }
    }

    public void SpawnBoss()
    {
        Instantiate(EnemyBossPrefab, new Vector3(Random.Range(-3, 3), 3, Random.Range(-3, 3)), Quaternion.Euler(0, 0, 0), GameManager.instance.Floor.currentRoomPrefab.transform);
    }

    public void EnemyDied()
    {
        NumberOfEnemies--;

        if(NumberOfEnemies == 0)
        {
            HasBeenCompleted = true;
            Decay.instance.HealDecay();
        }
    }
}
