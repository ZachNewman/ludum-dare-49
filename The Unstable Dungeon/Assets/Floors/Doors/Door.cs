﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door : MonoBehaviour
{
    public DoorSide doorSide;
    public enum DoorSide
    {
        NorthDoor,
        EastDoor,
        SouthDoor,
        WestDoor
    }

    private void OnTriggerEnter(Collider other)
    {
        if (!GameManager.instance.Floor.Layout[(int)GameManager.instance.Floor.CurrentRoomCoordinates.x, (int)GameManager.instance.Floor.CurrentRoomCoordinates.y].HasBeenCompleted)
            return;

        if (other.gameObject == PlayerStats.instance.gameObject)
        {
            if (PlayerStats.instance.timeSinceTeleport > 0)
                return;

            PlayerStats.instance.timeSinceTeleport = 0.1f;

            switch(doorSide)
            {
                case DoorSide.NorthDoor:
                    PlayerStats.instance.gameObject.transform.position = Vector3.forward * -10 + Vector3.up;
                    GameManager.instance.Floor.SpawnRoom((int)GameManager.instance.Floor.CurrentRoomCoordinates.x, (int)GameManager.instance.Floor.CurrentRoomCoordinates.y + 1);
                    break;

                case DoorSide.EastDoor:
                    PlayerStats.instance.gameObject.transform.position = Vector3.right * -10 + Vector3.up;
                    GameManager.instance.Floor.SpawnRoom((int)GameManager.instance.Floor.CurrentRoomCoordinates.x + 1, (int)GameManager.instance.Floor.CurrentRoomCoordinates.y);
                    break;

                case DoorSide.SouthDoor:
                    PlayerStats.instance.gameObject.transform.position = Vector3.forward * 10 + Vector3.up;
                    GameManager.instance.Floor.SpawnRoom((int)GameManager.instance.Floor.CurrentRoomCoordinates.x, (int)GameManager.instance.Floor.CurrentRoomCoordinates.y - 1);

                    break;

                case DoorSide.WestDoor:
                    PlayerStats.instance.gameObject.transform.position = Vector3.right * 10 + Vector3.up;
                    GameManager.instance.Floor.SpawnRoom((int)GameManager.instance.Floor.CurrentRoomCoordinates.x - 1, (int)GameManager.instance.Floor.CurrentRoomCoordinates.y);
                    break;
            }
            
            CameraController.instance.SnapToPlayer();

        }
    }
}
