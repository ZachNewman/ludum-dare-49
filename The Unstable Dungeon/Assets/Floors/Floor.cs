using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Reflection;

[CreateAssetMenu(fileName = "Floor", menuName = "Floor", order = 0)]
public class Floor : ScriptableObject
{
    public Room Room;

    public delegate void GameEvent();
    public static event GameEvent OnCreateFloor;
    GameObject RoomHolder;
    [HideInInspector] public Vector2 CurrentRoomCoordinates;
    [HideInInspector] public GameObject currentRoomPrefab;

    [Min(3)] public int NumberOfRegularRooms;
    [Min(0)] public int NumberOfTreasureRooms;
    [Min(0)] public int NumberOfShops;
    [Min(0)] public int NumberOfBossRooms;
    [Min(0)] public int NumberOfNPCRooms;
    [Min(3)] public int MaxWidth;
    [Min(3)] public int MaxHeight;
    [HideInInspector] public Room[,] Layout;
    [HideInInspector] public GameObject[,] TileLayout;
    [HideInInspector] public GameObject[,] RoomTypeLayout;


    List<Vector2> RoomsCreated = new List<Vector2>();
    Vector2 LayoutCoordinates;

    [ContextMenu("CreateFloor")]
    public void CreateFloor()
    {
        //initialise room
        Room.Initialise();

        //reset Rooms created
        RoomsCreated.Clear();

        //initialise new 2D array
        Layout = new Room[MaxWidth, MaxHeight];
        TileLayout = new GameObject[MaxWidth, MaxHeight];
        RoomTypeLayout = new GameObject[MaxWidth, MaxHeight];

        //set everything to no room
        for (int i = 0; i < MaxWidth; i++)
        {
            for (int j = 0; j < MaxHeight; j++)
            {
                Layout[i, j] = Instantiate(Room);
            }
        }

        //start at the centre
        LayoutCoordinates.x = MaxWidth / 2;
        LayoutCoordinates.y = MaxHeight / 2;

        //create the room
        Room.RoomType = RoomType.StartingRoom;
        Room.HasBeenCompleted = true;
        CurrentRoomCoordinates.x = (int)LayoutCoordinates.x;
        CurrentRoomCoordinates.y = (int)LayoutCoordinates.y;
        Layout[(int)LayoutCoordinates.x, (int)LayoutCoordinates.y] = Instantiate(Room);
        RoomsCreated.Add(LayoutCoordinates);

        //populate the other rooms
        CreateRoomsOfType(RoomType.RegularRoom, NumberOfRegularRooms + NumberOfTreasureRooms + NumberOfShops + NumberOfBossRooms);
        CreateRoomsOfType(RoomType.TreasureRoom, NumberOfTreasureRooms);
        CreateRoomsOfType(RoomType.Shop, NumberOfShops);
        CreateRoomsOfType(RoomType.BossRoom, NumberOfBossRooms);
        CreateRoomsOfType(RoomType.NPCRoom, NumberOfNPCRooms);

        //make the floor
        InstantiateFloor();

        SpawnRoom((int)CurrentRoomCoordinates.x, (int)CurrentRoomCoordinates.y);

        OnCreateFloor?.Invoke();
    }

    void CreateRoomsOfType(RoomType roomType, int numberOfRooms)
    {
        //change room type
        Room.RoomType = roomType;

        //change room completion
        Room.HasBeenCompleted = false;

        //reset number of rooms created
        int RoomCreationCounter = 0;

        //get a random room
        int randomRoom = Random.Range(0, RoomsCreated.Count);
        LayoutCoordinates = RoomsCreated[randomRoom];

        //keep populating rooms until we get to the appropriate number
        while (RoomCreationCounter < numberOfRooms)
        {
            //randomise number of enemies
            if (roomType == RoomType.RegularRoom)
            {
                Room.RandomiseNumberOfEnemies();
            }
            else if(roomType == RoomType.BossRoom)
            {
                Room.NumberOfEnemies = 1;
            }
            else
            {
                Room.NumberOfEnemies = 0;
            }

            //pick a random room
            randomRoom = Random.Range(0, RoomsCreated.Count);
            LayoutCoordinates = RoomsCreated[randomRoom];

            //choose a random dicection
            int randomDirection = Random.Range(0, 4);

            //add to the coordinates of the corresponding side. If it goes out of bounds, subtract it.
            if (roomType == RoomType.RegularRoom)
            {
                switch (randomDirection)
                {
                    case 0:
                        LayoutCoordinates.x++;
                        if (LayoutCoordinates.x > MaxWidth - 1)
                        {
                            LayoutCoordinates.x--;
                        }
                        break;

                    case 1:
                        LayoutCoordinates.x--;
                        if (LayoutCoordinates.x < 0)
                        {
                            LayoutCoordinates.x++;
                        }
                        break;

                    case 2:
                        LayoutCoordinates.y++;
                        if (LayoutCoordinates.y > MaxHeight - 1)
                        {
                            LayoutCoordinates.y--;
                        }
                        break;

                    case 3:
                        LayoutCoordinates.y--;
                        if (LayoutCoordinates.y < 0)
                        {
                            LayoutCoordinates.y++;
                        }
                        break;
                }
            }

            //If it can be placed, add it to the list of created rooms and increase the count
            if (CanBePlaced(roomType))
            {
                //Do the stats
                ChooseFactionStats();

                Layout[(int)LayoutCoordinates.x, (int)LayoutCoordinates.y] = Instantiate(Room);
                RoomsCreated.Add(new Vector2(LayoutCoordinates.x, LayoutCoordinates.y));
                RoomCreationCounter++;
            }
        }
    }

    void InstantiateFloor()
    {
        DestroyFloor();
        RoomHolder = new GameObject("Room Holder");
        RoomHolder.AddComponent<Canvas>();
        RoomHolder.transform.position = new Vector3(100, 100, 100);

        for (int i = 0; i < MaxWidth; i++)
        {
            for (int j = 0; j < MaxHeight; j++)
            {
                if (Layout[i, j].RoomType != RoomType.NoRoom)
                {
                    switch (Layout[i, j].Faction)
                    {
                        case Faction.Red:
                            TileLayout[i, j] = Instantiate(Room.RedPrefab, new Vector3(i, j, 0), Quaternion.Euler(0, 0, 0), RoomHolder.transform);
                            break;

                        case Faction.Blue:
                            TileLayout[i, j] = Instantiate(Room.BluePrefab, new Vector3(i, j, 0), Quaternion.Euler(0, 0, 0), RoomHolder.transform);
                            break;

                        case Faction.Yellow:
                            TileLayout[i, j] = Instantiate(Room.YellowPrefab, new Vector3(i, j, 0), Quaternion.Euler(0, 0, 0), RoomHolder.transform);
                            break;

                        case Faction.Black:
                            TileLayout[i, j] = Instantiate(Room.BlackPrefab, new Vector3(i, j, 0), Quaternion.Euler(0, 0, 0), RoomHolder.transform);
                            break;

                        case Faction.Purple:
                            TileLayout[i, j] = Instantiate(Room.PurplePrefab, new Vector3(i, j, 0), Quaternion.Euler(0, 0, 0), RoomHolder.transform);
                            break;
                    }
                }

                switch (Layout[i, j].RoomType)
                {
                    case RoomType.StartingRoom:
                        RoomTypeLayout[i, j] = Instantiate(Room.StartPrefab, new Vector3(i, j, 0), Quaternion.Euler(0, 0, 0), RoomHolder.transform);
                        break;

                    case RoomType.TreasureRoom:
                        RoomTypeLayout[i, j] = Instantiate(Room.TreasurePrefab, new Vector3(i, j, 0), Quaternion.Euler(0, 0, 0), RoomHolder.transform);
                        break;

                    case RoomType.Shop:
                        RoomTypeLayout[i, j] = Instantiate(Room.ShopPrefab, new Vector3(i, j, 0), Quaternion.Euler(0, 0, 0), RoomHolder.transform);
                        break;

                    case RoomType.BossRoom:
                        RoomTypeLayout[i, j] = Instantiate(Room.BossPrefab, new Vector3(i, j, 0), Quaternion.Euler(0, 0, 0), RoomHolder.transform);
                        break;

                    case RoomType.NPCRoom:
                        RoomTypeLayout[i, j] = Instantiate(Room.NPCPrefab, new Vector3(i, j, 0), Quaternion.Euler(0, 0, 0), RoomHolder.transform);
                        break;
                }
            }
        }
    }

    bool CanBePlaced(RoomType roomType)
    {
        if (Layout[(int)LayoutCoordinates.x, (int)LayoutCoordinates.y].RoomType != RoomType.NoRoom)
        {
            if (roomType == RoomType.RegularRoom)
            {
                return false;
            }
        }

        if (Layout[(int)LayoutCoordinates.x, (int)LayoutCoordinates.y].RoomType != RoomType.RegularRoom)
        {
            if (Room.IsSpecialRoom[roomType])
            {
                return false;
            }
        }

        int MaxPossibleNeighbours = 1;
        int NumberOfNeighbours = 0;

        if (LayoutCoordinates.x == 0)
        {
            return false;
        }
        else if (LayoutCoordinates.x == MaxWidth - 1)
        {
            return false;
        }
        else
        {
            if (Layout[(int)LayoutCoordinates.x + 1, (int)LayoutCoordinates.y].RoomType != RoomType.NoRoom)
            {
                NumberOfNeighbours++;
            }
            
            if (Layout[(int)LayoutCoordinates.x - 1, (int)LayoutCoordinates.y].RoomType != RoomType.NoRoom)
            {
                NumberOfNeighbours++;
            }

            if (Room.IsSpecialRoom[Layout[(int)LayoutCoordinates.x + 1, (int)LayoutCoordinates.y].RoomType])
            {
                if (Room.IsSpecialRoom[roomType])
                {
                    return false;
                }
            }

            if (Room.IsSpecialRoom[Layout[(int)LayoutCoordinates.x - 1, (int)LayoutCoordinates.y].RoomType])
            {
                if (Room.IsSpecialRoom[roomType])
                {
                    return false;
                }
            }
        }

        if (LayoutCoordinates.y == 0)
        {
            return false;
        }
        else if (LayoutCoordinates.y == MaxHeight - 1)
        {
            return false;
        }
        else
        {
            if (Layout[(int)LayoutCoordinates.x, (int)LayoutCoordinates.y + 1].RoomType != RoomType.NoRoom)
            {
                NumberOfNeighbours++;
            }
            
            if (Layout[(int)LayoutCoordinates.x, (int)LayoutCoordinates.y - 1].RoomType != RoomType.NoRoom)
            {
                NumberOfNeighbours++;
            }

            if (Room.IsSpecialRoom[Layout[(int)LayoutCoordinates.x, (int)LayoutCoordinates.y + 1].RoomType])
            {
                if (Room.IsSpecialRoom[roomType])
                {
                    return false;
                }
            }

            if (Room.IsSpecialRoom[Layout[(int)LayoutCoordinates.x, (int)LayoutCoordinates.y - 1].RoomType])
            {
                if (Room.IsSpecialRoom[roomType])
                {
                    return false;
                }
            }
        }

        if (NumberOfNeighbours <= MaxPossibleNeighbours)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    void ChooseFactionStats()
    {
        Room.FactionPower[Faction.Red] = LayoutCoordinates.x - (MaxWidth / 2) + LayoutCoordinates.y - (MaxHeight / 2);
        Room.FactionPower[Faction.Blue] = (MaxWidth / 2) - LayoutCoordinates.x + (MaxHeight / 2) - LayoutCoordinates.y;
        Room.FactionPower[Faction.Yellow] = (MaxWidth / 2) - LayoutCoordinates.x + LayoutCoordinates.y - (MaxHeight / 2);
        Room.FactionPower[Faction.Black] = LayoutCoordinates.x - (MaxWidth / 2) + (MaxHeight / 2) - LayoutCoordinates.y;

        Room.FactionPower[Faction.Red] += MaxHeight/2 + MaxWidth/2;
        Room.FactionPower[Faction.Blue] += MaxHeight/2 + MaxWidth/2;
        Room.FactionPower[Faction.Yellow] += MaxHeight/2 + MaxWidth/2;
        Room.FactionPower[Faction.Black] += MaxHeight/2 + MaxWidth/2;

        Room.FactionPower[Faction.Red] *= Random.Range(1, 1.5f);
        Room.FactionPower[Faction.Blue] *= Random.Range(1, 1.5f);
        Room.FactionPower[Faction.Yellow] *= Random.Range(1, 1.5f);
        Room.FactionPower[Faction.Black] *= Random.Range(1, 1.5f);

        Room.FactionPower[Faction.Purple] = MaxWidth / 2 - Mathf.Abs(MaxWidth / 2 - LayoutCoordinates.x) + MaxHeight / 2 - Mathf.Abs(MaxHeight / 2 - LayoutCoordinates.y);
        Room.FactionPower[Faction.Purple] += Room.NeutralThreshHold;

        Room.DeclareFaction();
    }

    [ContextMenu("Destroy Floor")]
    public void DestroyFloor()
    {
        if (RoomHolder != null)
        {
            Destroy(RoomHolder.gameObject);
        }
    }

    public void SpawnRoom(int XCoordinate, int YCoordinate)
    {
        Destroy(currentRoomPrefab?.gameObject);

        CurrentRoomCoordinates = new Vector2(XCoordinate, YCoordinate);
        currentRoomPrefab = Instantiate(Room.RoomPrefab, Vector3.zero, Quaternion.Euler(0, 0, 0));

        if(Layout[XCoordinate + 1, YCoordinate].RoomType != RoomType.NoRoom)
        {
            Instantiate(Room.EastDoorPrefab, Vector3.right * 14, Quaternion.Euler(0,0,0), currentRoomPrefab.transform);
        }

        if (Layout[XCoordinate - 1, YCoordinate].RoomType != RoomType.NoRoom)
        {
            Instantiate(Room.WestDoorPrefab, Vector3.right * -14, Quaternion.Euler(0, 0, 0), currentRoomPrefab.transform);
        }

        if (Layout[XCoordinate, YCoordinate + 1].RoomType != RoomType.NoRoom)
        {
            Instantiate(Room.NorthDoorPrefab, Vector3.forward * 14, Quaternion.Euler(0, 0, 0), currentRoomPrefab.transform);
        }

        if (Layout[XCoordinate, YCoordinate - 1].RoomType != RoomType.NoRoom)
        {
            Instantiate(Room.SouthDoorPrefab, Vector3.forward * -14, Quaternion.Euler(0, 0, 0), currentRoomPrefab.transform);
        }

        if (!Layout[XCoordinate, YCoordinate].HasBeenCompleted)
        {
            if (Layout[XCoordinate, YCoordinate].RoomType == RoomType.RegularRoom)
            {
                Layout[XCoordinate, YCoordinate].SpawnEnemies();
            }
                
            if (Layout[XCoordinate, YCoordinate].RoomType == RoomType.BossRoom)
            {
                Layout[XCoordinate, YCoordinate].SpawnBoss();
            }
        }

        GameManager.instance.SnapToCurrentRoom();
    }

    public void DestroyRandomRoom()
    {
        int Rand = Random.Range(0, RoomsCreated.Count);

        if (RoomsCreated[Rand] == CurrentRoomCoordinates)
            return;

        if (RoomsCreated[Rand].x == CurrentRoomCoordinates.x + 1 && RoomsCreated[Rand].y == CurrentRoomCoordinates.y)
            return;

        if (RoomsCreated[Rand].x == CurrentRoomCoordinates.x - 1 && RoomsCreated[Rand].y == CurrentRoomCoordinates.y)
            return;

        if (RoomsCreated[Rand].x == CurrentRoomCoordinates.x && RoomsCreated[Rand].y == CurrentRoomCoordinates.y + 1)
            return;

        if (RoomsCreated[Rand].x == CurrentRoomCoordinates.x && RoomsCreated[Rand].y == CurrentRoomCoordinates.y - 1)
            return;

        if (RoomsCreated.Count < 10)
            return;

        //RoomsCreated.RemoveAt(Rand);
        Layout[(int)RoomsCreated[Rand].x, (int)RoomsCreated[Rand].y].RoomType = RoomType.NoRoom;
        Destroy(TileLayout[(int)RoomsCreated[Rand].x, (int)RoomsCreated[Rand].y]);
        Destroy(RoomTypeLayout[(int)RoomsCreated[Rand].x, (int)RoomsCreated[Rand].y]);
        RoomsCreated.RemoveAt(Rand);

    }
}
