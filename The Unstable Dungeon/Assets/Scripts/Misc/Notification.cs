﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

//[RequireComponent(typeof(TextMeshProUGUI))]
public class Notification : MonoBehaviour
{
    [SerializeField] TextMeshProUGUI text;

    [SerializeField] float textLifeTime;
    float timer;

    public static Notification instance;
    private void Awake()
    {
        instance = this;
    }

    public void SetText(string t)
    {
        timer = textLifeTime;

        text.gameObject.SetActive(true);

        text.SetText(t);
    }

    private void Update()
    {
        timer -= Time.deltaTime;


        if(timer/textLifeTime <= 0.5f)
            text.faceColor = new Color32(255,255,255, (byte)(255 *(timer / (textLifeTime/2))));
        else
            text.faceColor = new Color32(255, 255, 255, 255);

        text.gameObject.SetActive(timer > 0);
    }
}
