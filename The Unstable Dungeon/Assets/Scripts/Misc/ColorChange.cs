﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

public class ColorChange : MonoBehaviour
{
    TextMeshProUGUI text;

    float timer;

    private void Awake()
    {
        text = GetComponent<TextMeshProUGUI>();
    }

    // Update is called once per frame
    void Update()
    {
        timer -= Time.deltaTime;

        if(timer <= 0)
        {
            if (text)
            {
                text.faceColor = new Color32(
                    (byte)Random.Range(0, 255),
                    (byte)Random.Range(0, 255),
                    (byte)Random.Range(0, 255),
                    255);
            }

            timer = 0.5f;
        }
    }

    public void B_Menu()
    {
        SceneManager.LoadScene(0);
    }
    public void B_Quit()
    {
        Application.Quit();
    }
}
