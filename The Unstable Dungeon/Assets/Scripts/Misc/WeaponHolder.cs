﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponHolder : MonoBehaviour
{
    public GroundedItem itemFrontEndPrefab;


    [Header("Avaialble Items")]
    public List<Weapon> availableWeapons;

    public List<Status> availableBuffs;

    public static WeaponHolder instance;
    private void Awake()
    {
        instance = this;
    }

}
