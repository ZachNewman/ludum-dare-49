﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Image))]
public class HealthBar : MonoBehaviour
{
    Image image;

    private void Awake()
    {
        image = GetComponent<Image>();
    }

    // Update is called once per frame
    void Update()
    {
        if(PlayerStats.instance)
            image.fillAmount = PlayerStats.instance.currentHealth / PlayerStats.instance.GetMaxHealth();
    }
}
