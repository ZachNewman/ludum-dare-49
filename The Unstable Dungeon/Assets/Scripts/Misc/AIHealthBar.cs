﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Image))]
public class AIHealthBar : MonoBehaviour
{
    CharacterStats stats;

    Image image;

    private void Awake()
    {
        stats = GetComponentInParent<CharacterStats>();

        image = GetComponent<Image>();
    }

    // Update is called once per frame
    void Update()
    {
        image.fillAmount = stats.currentHealth / stats.GetMaxHealth();
    }
}
