﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Buff", menuName = "Status/Buff")]
public class Buff : Status
{
    [Space]
    [SerializeField] StatusEffect effect;
    [Space]
    [SerializeField] float value;

    public override void ApplyEffect(CharacterStats c, CharacterStats caster = null)
    {
        base.ApplyEffect(c, caster);

        c.effects[effect] = c.GetEffect(effect) + value;
    }

    public override void UpdateEffect(CharacterStats c, CharacterStats caster = null)
    {
        base.UpdateEffect(c, caster);
    }

    public override void RemoveEffect(CharacterStats c, CharacterStats caster = null)
    {
        base.RemoveEffect(c, caster);

        if (effect == StatusEffect.invinsibility)
            return;

        c.effects[effect] = c.GetEffect(effect) - value;
    }
}
