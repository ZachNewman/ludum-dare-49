﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Status : Item
{
    public float duration;
    [Space]
    [SerializeField] protected bool eternal = false;

    [Space]

    public GameObject groundParticles;

    public float GetTimer() { return timer; }
    public void ResetTimer() { timer = 0; }
    public void IncreaseTimer(float delta)
    {
        timer += delta;
    }

    float timer = 0;

    public virtual void ApplyEffect(CharacterStats c, CharacterStats caster = null)
    {

    }
    public virtual void UpdateEffect(CharacterStats c, CharacterStats caster = null)
    {
        if (eternal)
            ResetTimer();
    }
    public virtual void RemoveEffect(CharacterStats c, CharacterStats caster = null)
    {

    }
}
