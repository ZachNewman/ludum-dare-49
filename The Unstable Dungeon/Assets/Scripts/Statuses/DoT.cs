﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "DoT", menuName = "Status/DoT")]
public class DoT : Status
{
    public float dmgPerTick;
    [SerializeField] float frequency;
    float internalTimer;

    [Space]
    [SerializeField] int maxTicks;
    int ticksOccured;

    public override void ApplyEffect(CharacterStats c, CharacterStats caster = null)
    {
        base.ApplyEffect(c, caster);
    }

    public override void UpdateEffect(CharacterStats c, CharacterStats caster = null)
    {
        base.UpdateEffect(c, caster);

        if (maxTicks != 0 && ticksOccured >= maxTicks)
            return;

        internalTimer += Time.deltaTime;

        if (internalTimer >= frequency)
        {
            c.TakeDamage(dmgPerTick);

            ticksOccured++;

            internalTimer = 0;
        }
    }

    public override void RemoveEffect(CharacterStats c, CharacterStats caster = null)
    {
        base.RemoveEffect(c, caster);
    }
}
