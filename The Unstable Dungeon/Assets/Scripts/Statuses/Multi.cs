﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Multi", menuName = "Status/Multi")]
public class Multi : Status
{
    [SerializeField] Status[] statuses;

    List<Status> appliedStatuses = new List<Status>();

    public override void ApplyEffect(CharacterStats c, CharacterStats caster = null)
    {
        base.ApplyEffect(c, caster);

        foreach (Status s in statuses)
        {
            appliedStatuses.Add(c.AddStatus(s));
        }
    }

    public override void UpdateEffect(CharacterStats c, CharacterStats caster = null)
    {
        base.UpdateEffect(c, caster);
    }

    public override void RemoveEffect(CharacterStats c, CharacterStats caster = null)
    {
        base.RemoveEffect(c, caster);

        foreach (Status s in appliedStatuses)
        {
            if (s)
            {
                c.RemoveStatus(s);
            }
        }
    }
}
