﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CharacterStats), typeof(CombatController))]
public class BossAttacks : MonoBehaviour
{
    [SerializeField] float timeBetweenMegaAttacks;
    float timer;

    [SerializeField] AttackAction[] megaAttacks;

    CharacterStats statistics;
    CombatController combatController;

    private void Awake()
    {
        statistics = GetComponent<CharacterStats>();
        combatController = GetComponent<CombatController>();
    }

    void Start()
    {
        timer = timeBetweenMegaAttacks;
    }

    void Update()
    {
        if (statistics.IsDead())
            return;

        timer -= Time.deltaTime;

        if(timer <= 0)
        {
            timer = timeBetweenMegaAttacks;
            DoMegaAttack();
        }
    }

    void DoMegaAttack()
    {
        if (megaAttacks.Length == 0)
            return;

        int rand = Random.Range(0, megaAttacks.Length);

        if (megaAttacks[rand] == null)
            return;

        AttackAction aa = Instantiate(megaAttacks[rand].gameObject, combatController.attackPoint).GetComponent<AttackAction>();

        if (!aa.shouldFollowPlayer)
            aa.transform.SetParent(null);

        aa.Initialise(statistics, combatController.GetHeldWeapon(), true);
    }
}
