﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CharacterStats))]
public class StartWithBuffs : MonoBehaviour
{
    CharacterStats statistics;

    [SerializeField] Status[] possibleStartingBuffs;
    [SerializeField] int numOfBuffs;

    private void Awake()
    {
        statistics = GetComponent<CharacterStats>();
    }

    // Start is called before the first frame update
    void Start()
    {
        for (int i = 0; i < numOfBuffs; i++)
        {
            statistics.AddStatus(possibleStartingBuffs[Random.Range(0, possibleStartingBuffs.Length)]);
        }

        statistics.TakeDamage(-100000);
    }
}
