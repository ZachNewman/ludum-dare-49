﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CharacterStats), typeof(CombatController), typeof(CharacterController))]
public class AiController : MonoBehaviour
{
    CombatController combatController;
    CharacterStats statistics;

    [SerializeField] float rotateSpeed;

    [Space]

    float fleePercent;

    [SerializeField] float fleeSpeedReduction = 0.8f;

    float strikeCooldownTime = 0.3f;
    float timer;

    CharacterController cc;
    Vector3 velocity;

    private void Awake()
    {
        statistics = GetComponent<CharacterStats>();
        combatController = GetComponent<CombatController>();
        cc = GetComponent<CharacterController>();
    }

    private void Start()
    {
        //Randomize the stats
        if (Random.Range(0, 3) == 0)
            fleePercent = 0;
        else
        {
            fleePercent = Random.Range(0, 100) / 100;
        }

        if (WeaponHolder.instance != null)
        {
            if (WeaponHolder.instance.availableWeapons.Count == 0)
                Debug.LogError("No Weapons for the AI to grab");
            else
                combatController.EquipWeapon(WeaponHolder.instance.availableWeapons[Random.Range(0, WeaponHolder.instance.availableWeapons.Count)]);
        }

        strikeCooldownTime = Random.Range(0.3f, 1);

        timer = strikeCooldownTime;
    }

    private void OnEnable()
    {
        statistics.OnCharDeath += DestroySelf;
    }
    private void OnDisable()
    {
        statistics.OnCharDeath -= DestroySelf;
    }


    // Update is called once per frame
    void Update()
    {
        if (PlayerStats.instance.IsDead() || statistics.IsDead())
            return;
        
        //Rotate towards player
        Vector3 playerDir = (PlayerStats.instance.transform.position - transform.position).normalized;
        playerDir.y = 0;
        transform.rotation = Quaternion.LookRotation(Vector3.RotateTowards(transform.forward, playerDir, rotateSpeed * Time.deltaTime, 0.0f));
        
        float distToPlayer = Vector3.Distance(transform.position, PlayerStats.instance.transform.position);

        //If no weapon or health below a percentage
        if(combatController.GetHeldWeapon() == null || statistics.currentHealth / statistics.GetMaxHealth() < fleePercent)
        {
            //Default to flee state
            velocity = -playerDir * statistics.GetMoveSpeed() * fleeSpeedReduction;

            statistics.GetAnimator().SetFloat("IdleBlend", fleeSpeedReduction);
        }
        else
        {
            if (distToPlayer > combatController.GetHeldWeapon().idealRange)
            {
                //move towards the player
                velocity = playerDir * statistics.GetMoveSpeed();

                statistics.GetAnimator().SetFloat("IdleBlend", 1);


                if (distToPlayer < combatController.GetHeldWeapon().idealRange + combatController.GetHeldWeapon().swingOffset)
                {
                    timer -= Time.deltaTime;

                    if (timer <= 0)
                    {
                        combatController.Attack();
                        timer = strikeCooldownTime + combatController.GetHeldWeapon().attackSpeed;
                    }
                }
            }
            else
            {
                if (distToPlayer < combatController.GetHeldWeapon().retreatRange)
                {
                    //run back
                    velocity = -playerDir * statistics.GetMoveSpeed() * fleeSpeedReduction;

                    statistics.GetAnimator().SetFloat("IdleBlend", fleeSpeedReduction);

                }
                else
                {
                    statistics.GetAnimator().SetFloat("IdleBlend", 0);

                    timer -= Time.deltaTime;

                    if (timer <= 0)
                    {
                        combatController.Attack();
                        timer = strikeCooldownTime + combatController.GetHeldWeapon().attackSpeed;
                    }

                    velocity = Vector3.zero;
                }
            }
        }
        velocity += Physics.gravity;


        cc.Move(velocity * Time.deltaTime);
    }

    void DestroySelf()
    {
        Destroy(gameObject, 2);
    }
}
