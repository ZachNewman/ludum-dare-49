﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class King : MonoBehaviour
{
    CharacterStats stats;

    private void Awake()
    {
        stats = GetComponent<CharacterStats>();
    }

    private void OnEnable()
    {
        stats.OnCharDeath += WinGame;
    }
    private void OnDisable()
    {
        stats.OnCharDeath -= WinGame;
    }

    // Start is called before the first frame update
    void Start()
    {
        AudioManager.instance.PauseMusic();
        Time.timeScale = 0;
        transform.position = new Vector3(100, 100, 100);
        StartCoroutine(PlayMusic());
    }

    IEnumerator PlayMusic()
    {
        yield return new WaitForSecondsRealtime(3);
        AudioManager.instance.PlayMusic();
        Time.timeScale = 1;
        transform.position = new Vector3(0, 2, 0);
    }

    public void WinGame()
    {
        StartCoroutine(WaitBeforeDeath());
    }

    IEnumerator WaitBeforeDeath()
    {
        yield return new WaitForSecondsRealtime(1);
        SceneManager.LoadScene(2);

    }
}
