﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum StatusEffect
{
    speed,
    maxHealth,
    damage,
    attackSpeed,
    invinsibility,
    lifeSteal,
    stunned,
}

public class CharacterStats : MonoBehaviour
{
    [Header("Stats")]
    [SerializeField] protected float maxHealth;
    public float currentHealth;
    [Space]
    [SerializeField] float baseMoveSpeed;

    public Dictionary<StatusEffect, float> effects = new Dictionary<StatusEffect, float>();

    //[HideInInspector]
    public List<Status> statuses = new List<Status>();

    [Header("Visual FX")]
    [SerializeField] GameObject buffFX;

    [SerializeField] Transform rootBone;
    Rigidbody[] ragdollParts;

    [SerializeField] ParticleSystem hitParticles;

    //Delegates
    public delegate void OnDie();
    public event OnDie OnCharDeath;

    Animator animator;
    public Animator GetAnimator() { return animator; }

    protected virtual void Awake()
    {
        currentHealth = GetMaxHealth();
        animator = GetComponentInChildren<Animator>();

        if (rootBone)
        {
            ragdollParts = rootBone.GetComponentsInChildren<Rigidbody>();

            foreach (Rigidbody rb in ragdollParts)
            {
                rb.isKinematic = true;
            }
        }

        effects.Add(StatusEffect.invinsibility, 0);
        effects.Add(StatusEffect.stunned, 0);
    }

    // Update is called once per frame
    protected virtual void Update()
    {
        List<Status> deathrow = new List<Status>();
        foreach (Status s in statuses)
        {
            s.UpdateEffect(this);
            s.IncreaseTimer(Time.deltaTime);

            if (s.GetTimer() > s.duration)
                deathrow.Add(s);
        }

        foreach (Status s in deathrow)
        {
            statuses.Remove(s);

            s.RemoveEffect(this);
        }

        if (GetEffect(StatusEffect.invinsibility) > 0)
            effects[StatusEffect.invinsibility] -= Time.deltaTime;

        if (GetEffect(StatusEffect.stunned) > 0)
            effects[StatusEffect.stunned] -= Time.deltaTime;
    }

    //Add a status to us
    public Status AddStatus(Status s, CharacterStats caster = null)
    {
        Status inst = Instantiate(s);

        inst.name = s.name;
        
        statuses.Add(inst);
        inst.ApplyEffect(this, caster);

        return inst;
    }

    //Remove a status
    public void RemoveStatus(Status s)
    {
        if (statuses.Contains(s))
        {
            statuses.Remove(s);

            s.RemoveEffect(this);
        }
    }

    //Can pass in a negativeValue to heal
    public void TakeDamage(float dmg)
    {
        if (GetEffect(StatusEffect.invinsibility) > 0 && dmg > 0)
            return;

        currentHealth -= dmg;

        if (dmg > 0)
        {
            if (hitParticles)
            {
                ParticleSystem go = Instantiate(hitParticles, transform);
                Destroy(go.gameObject, go.duration);
            }
        }

        if (currentHealth <= 0)
            Die();

        if (currentHealth > GetMaxHealth())
            currentHealth = GetMaxHealth();
    }

    [ContextMenu("KILL URSELF")]
    //Function to kill us
    public void Die()
    {
        if(OnCharDeath != null)
            OnCharDeath.Invoke();

        if (animator)
            animator.enabled = false;

        if(ragdollParts != null)
        {
            foreach (Rigidbody rb in ragdollParts)
            {
                rb.isKinematic = false;
                rb.gameObject.layer = 12;
            }
        }

        //if not player
        if (gameObject != PlayerStats.instance.gameObject)
        {
            //spawn loot
            if (WeaponHolder.instance.itemFrontEndPrefab != null)
            {
                GameObject go = Instantiate(
                    WeaponHolder.instance.itemFrontEndPrefab.gameObject,
                    transform.position,
                    Quaternion.identity);

                go.GetComponent<GroundedItem>().Initialise();

                if(GameManager.instance)
                    go.transform.SetParent(GameManager.instance.Floor.currentRoomPrefab.transform);

                go.transform.localScale = new Vector3(0.33f, 0.33f, 0.33f);

                Rigidbody rb = go.GetComponent<Rigidbody>();

                if (rb)
                {
                    rb.AddForce(Vector3.up * 100);

                    Vector3 dir = new Vector3(Random.Range(-300,300), 0, Random.Range(-300, 300));
                    rb.AddForce(dir);
                }
                
            }

            //progress clearing room
            if(GameManager.instance)
                GameManager.instance.Floor.Layout[(int)GameManager.instance.Floor.CurrentRoomCoordinates.x, (int)GameManager.instance.Floor.CurrentRoomCoordinates.y].EnemyDied();
        }

        this.enabled = false;
    }

    //Returns true if dead
    public bool IsDead() { return currentHealth <= 0; }

    public void PlayerBuffed()
    {
        if (buffFX)
        {
            GameObject go = Instantiate(buffFX, transform);

            Destroy(go, 5);
        }
    }

    //Get an effect of any kind applied to us
    public float GetEffect(StatusEffect effect)
    {
        if (effects.ContainsKey(effect))
            return effects[effect];

        return 0;
    }

    //Get our different statistics
    public float GetMaxHealth() { return maxHealth + GetEffect(StatusEffect.maxHealth); }
    public float GetMoveSpeed() { return baseMoveSpeed + GetEffect(StatusEffect.speed); }
}
