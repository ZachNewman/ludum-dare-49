﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

[RequireComponent(typeof(CharacterStats), typeof(CombatController))]
public class PickUp : MonoBehaviour
{
    [SerializeField] LayerMask pickupMask;
    [SerializeField] float range;

    CharacterStats statistics;
    CombatController combatController;

    private void Awake()
    {
        statistics = GetComponent<CharacterStats>();
        combatController = GetComponent<CombatController>();
    }

    public void OnPickUp(InputAction.CallbackContext value)
    {
        if (value.started)
        {
            PickUpNearby();
        }
    }

    void PickUpNearby()
    {
        Collider[] hits = Physics.OverlapSphere(transform.position, range, pickupMask);

        GroundedItem closestItem = null;

        foreach(Collider c in hits)
        {
            GroundedItem i = c.GetComponent<GroundedItem>();

            if (i)
            {
                if (closestItem == null)
                {
                    closestItem = i;
                    continue;
                }

                if (Vector3.Distance(transform.position, i.transform.position) < Vector3.Distance(transform.position, closestItem.transform.position))
                {
                    closestItem = i;
                }
            }
        }

        if (!closestItem)
            return;

        Weapon w = closestItem.item as Weapon;
        if (w)
        {
            combatController.EquipWeapon(w);

            Destroy(closestItem.gameObject);

            return;
        }

        Status si = closestItem.item as Status;
        if (si)
        {
            statistics.AddStatus(si, null);

            statistics.PlayerBuffed();

            Destroy(closestItem.gameObject);

            Notification.instance.SetText(si.description);

            return;
        }
    }
}
