﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    [SerializeField] Vector3 offset;

    [SerializeField] float smoothTime;

    bool shouldFollow = true;

    public void SetFollow(bool val) { shouldFollow = val; }

    Vector3 velocity;

    public static CameraController instance;
    private void Awake()
    {
        if(instance == null)
            instance = this;
    }

    private void Start()
    {
        SnapToPlayer();

        transform.LookAt(PlayerStats.instance.transform);
    }

    void Update()
    {
        if (shouldFollow)
        {
            if(PlayerStats.instance != null)
            {
                float dampening = smoothTime == 0 ? smoothTime : 1 / smoothTime;

                transform.position = Vector3.SmoothDamp(transform.position, PlayerStats.instance.transform.position + offset, ref velocity, dampening);
            }
        }
    }

    public void SnapToPlayer()
    {
        transform.position = PlayerStats.instance.transform.position + offset;

    }
}
