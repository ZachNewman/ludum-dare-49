﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;

[RequireComponent(typeof(CharacterController), typeof(CharacterStats))]
public class CharController : MonoBehaviour
{
    Vector2 movementInput;
    //[SerializeField]
    Vector2 lookInput;

    //[SerializeField]
    Vector3 velocity = new Vector3();

    CharacterController cc;
    CharacterStats statistics;

    PlayerInput inputManager;

    Animator animator;
    public Animator GetAnimator() { return animator; }

    private void Awake()
    {
        cc = GetComponent<CharacterController>();
        statistics = GetComponent<CharacterStats>();

        animator = GetComponentInChildren<Animator>();

        inputManager = GetComponent<PlayerInput>();
    }

    private void OnEnable()
    {
        statistics.OnCharDeath += delegate { cc.enabled = false; };
    }
    private void OnDisable()
    {
        statistics.OnCharDeath -= delegate { cc.enabled = false; };
    }

    void Update()
    {
        //If dead, don't move
        if (statistics.IsDead())
        {
            return;
        }

        if (statistics.GetEffect(StatusEffect.stunned) > 0)
            return;

        //Handles Movement
        #region Movement
        //if we are on the ground
        if (cc.isGrounded)
        {
            //Set our velocity to use our input
            velocity = Vector3.forward * movementInput.y * statistics.GetMoveSpeed()
                + Vector3.right * movementInput.x * statistics.GetMoveSpeed()
                + Physics.gravity;
        }

        velocity.y += Physics.gravity.y * Time.deltaTime;

        cc.Move(velocity * Time.deltaTime);

        if(animator)
            animator.SetFloat("IdleBlend", movementInput.magnitude);

        #endregion

        //Handles rotation
        #region Rotation
        if (inputManager)
        {
            //if on the gamepad
            if(inputManager.currentControlScheme == "GamePad")
            {
                Vector3 forward = new Vector3(lookInput.x, 0, lookInput.y).normalized;

                if (forward != Vector3.zero)
                    transform.forward = forward;
            }
            //if on keyboard/mouse
            else
            {
                Ray ray = Camera.main.ScreenPointToRay(Mouse.current.position.ReadValue());
                RaycastHit hit;

                if(Physics.Raycast(ray, out hit))
                {
                    Vector3 forward = (hit.point - transform.position).normalized;
                    forward.y = 0;

                    if (forward != Vector3.zero)
                        transform.forward = forward;
                }

                
            }
        }
        
        #endregion
    }

    public void OnMovement(InputAction.CallbackContext value)
    {
        movementInput = value.ReadValue<Vector2>();
    }
    public void OnLook(InputAction.CallbackContext value)
    {
        lookInput = value.ReadValue<Vector2>();
    }

    public void OnDungeonReset(InputAction.CallbackContext value)
    {
        if(value.started)
        {
            SceneManager.LoadScene(1);
        }    
    }
}
