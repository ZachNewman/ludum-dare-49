﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundedItem : MonoBehaviour
{
    public Item item;

    public void Initialise(Item spawnedItem = null)
    {
        item = spawnedItem;

        //randomise the item
        if (item == null)
        {
            if (Random.Range(0, 4) <= 2)
            {
                //Spawn a buff
                item = WeaponHolder.instance.availableBuffs[Random.Range(0, WeaponHolder.instance.availableBuffs.Count)];
            }
            else
            {
                //Spawn a weapon
                item = WeaponHolder.instance.availableWeapons[Random.Range(0, WeaponHolder.instance.availableWeapons.Count)];
            }
            
        }
        

        Status s = item as Status;

        if (s)
        {
            if (s.groundParticles != null)
            {
                GameObject go = Instantiate(s.groundParticles, transform);
            }

            return;
        }

        Weapon w = item as Weapon;

        if (w)
        {
            if (w.mesh != null)
            {
                GameObject go = Instantiate(w.mesh, transform);
            }

            return;
        }
    }
}
