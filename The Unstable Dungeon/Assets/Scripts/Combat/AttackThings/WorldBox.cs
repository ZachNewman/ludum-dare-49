﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorldBox : AttackAction
{
    private void Awake()
    {
        Initialise(null, null, true);
    }

    private void OnTriggerEnter(Collider other)
    {
        CharacterStats cs;
        if (other.TryGetComponent(out cs))
        {
            if(cs.IsDead() == false)
                cs.TakeDamage(damage);
        }
    }
}
