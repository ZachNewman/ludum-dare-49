﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SummonMinion : AttackAction
{
    [SerializeField] GameObject minion;
    [SerializeField] Vector2Int numToSpawn;

    [Space]

    [SerializeField] GameObject vfx;

    public override void Initialise(CharacterStats caster, Weapon w, bool overrideWpn = false)
    {
        base.Initialise(caster, w, overrideWpn);

        int num = Random.Range(numToSpawn.x, numToSpawn.y);

        for (int i = 0; i < num; i++)
        {
            Vector3 spot = new Vector3(Random.Range(-8, 8), 1, Random.Range(-8, 8));

            Instantiate(minion, spot, Quaternion.identity, GameManager.instance.Floor.currentRoomPrefab.transform);

            GameManager.instance.Floor.Layout[(int)GameManager.instance.Floor.CurrentRoomCoordinates.x, (int)GameManager.instance.Floor.CurrentRoomCoordinates.y].NumberOfEnemies++;

            if (vfx)
            {
                GameObject go = Instantiate(vfx, spot, Quaternion.identity, GameManager.instance.Floor.currentRoomPrefab.transform);
                Destroy(go, 3);
            }
        }

        End();
    }
}
