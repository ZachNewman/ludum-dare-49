﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TeleportAtk : AttackAction
{
    [SerializeField] AttackAction secondaryAttack;

    [SerializeField] GameObject teleportParticles;
    [SerializeField] GameObject slamParticles;

    public override void Initialise(CharacterStats caster, Weapon w, bool overrideWpn = false)
    {
        base.Initialise(caster, w, overrideWpn);

        //Teleport
        Vector3 RandPos = new Vector3(
            Random.Range(-8,8), 
            1, 
            Random.Range(-8,8));

        if (teleportParticles)
        {
            GameObject teleportPart = Instantiate(teleportParticles, caster.transform.position, caster.transform.rotation);
            Destroy(teleportPart, 5);
        }

        caster.transform.position = RandPos;
        

        AttackAction aa = Instantiate(secondaryAttack.gameObject, caster.GetComponent<CombatController>().attackPoint).GetComponent<AttackAction>();

        if (aa.spawnOnCaster)
            aa.transform.position = caster.transform.position;

        if (aa.shouldFollowPlayer == false)
            aa.transform.SetParent(null);

        aa.Initialise(caster, w, overrideWpn);

        if (slamParticles)
        {
            GameObject slamPart = Instantiate(slamParticles, caster.transform.position, caster.transform.rotation);
            Destroy(slamPart, 5);
        }
        End();
    }
}
