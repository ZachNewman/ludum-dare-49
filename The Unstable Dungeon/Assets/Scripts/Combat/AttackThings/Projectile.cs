﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody), typeof(Collider))]
public class Projectile : AttackAction
{
    [SerializeField] float projectileSpeed;

    [SerializeField] bool useGravity;

    [SerializeField] float range;

    Rigidbody rb;

    [SerializeField] LayerMask destroyMask;

    Vector3 startPos;

    private void Awake()
    {
        rb = GetComponent<Rigidbody>();
    }

    public override void Initialise(CharacterStats _caster, Weapon w, bool overrideWpn = false)
    {
        base.Initialise(_caster, w, overrideWpn);

        rb.useGravity = useGravity;

        rb.AddForce(transform.forward * projectileSpeed);

        startPos = transform.position;
    }

    protected override void Update()
    {
        base.Update();

        if (Vector3.Distance(transform.position, startPos) > range)
            End();
    }

    private void OnTriggerEnter(Collider other)
    {
        CharacterStats cs;
        if (other.TryGetComponent(out cs))
        {
            if (!hitSomething)
            {
                if (cs != caster)
                {
                    if(cs.IsDead() == false)
                    {
                        cs.TakeDamage(damage);

                        if (status)
                        {
                            cs.AddStatus(status, caster);
                        }

                        if (endOnHit)
                        {
                            hitSomething = true;

                            End();
                        }
                    }
                }
            }
        }
        else
        {
            //If what we hit is something which wants to destroy us
            if (destroyMask == (destroyMask | (1 << other.gameObject.layer)))
            {
                End();
            }
        }
    }

    public override void End()
    {
        foreach (Transform child in transform)
        {
            ParticleSystem ps = child.GetComponent<ParticleSystem>();
            if (ps)
            {
                ps.enableEmission = false;
                ps.transform.SetParent(null);
                Destroy(ps.gameObject, 3);
            }
        }

        base.End();
    }
}
