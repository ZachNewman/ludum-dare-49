﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class AttackAction : MonoBehaviour
{
    protected CharacterStats caster;
    [SerializeField] protected float lifeTime;
    protected float timer = 0;

    [Space]

    [SerializeField] protected Status status;

    [SerializeField] protected bool timeLessAttack = false;
    [SerializeField] protected bool endOnHit = true;

    public bool spawnOnCaster;

    public bool shouldFollowPlayer;


    public float damage;
    protected bool hitSomething = false;

    public virtual void Initialise(CharacterStats _caster, Weapon w, bool overrideWpn = false)
    {
        caster = _caster;

        if(overrideWpn == false)
            damage = w.damage + caster.GetEffect(StatusEffect.damage);

        if (!shouldFollowPlayer)
        {
            transform.SetParent(null);
        }
    }

    public virtual void End()
    {
        Destroy(this.gameObject);
    }

    protected virtual void Update()
    {
        if (timeLessAttack)
            return;

        timer += Time.deltaTime;

        if (timer > lifeTime)
            End();
    }
}
