﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider))]
public class Melee : AttackAction
{
    public override void Initialise(CharacterStats caster, Weapon w, bool overrideWpn = false)
    {
        base.Initialise(caster, w, overrideWpn);
    }

    protected override void Update()
    {
        base.Update();
    }

    private void OnTriggerEnter(Collider other)
    {
        CharacterStats cs;
        if (other.TryGetComponent(out cs))
        {
            if (!hitSomething)
            {
                if (cs != caster)
                {
                    if(cs.IsDead() == false)
                    {
                        caster.TakeDamage(-caster.GetEffect(StatusEffect.lifeSteal));

                        cs.TakeDamage(damage);

                        if (status)
                        {
                            cs.AddStatus(status, caster);
                        }

                        if (endOnHit)
                        {
                            hitSomething = true;

                            End();
                        }
                    }
                }
            }
        }
    }
}
