﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FreezePlayer : AttackAction
{
    [Space]
    [SerializeField] Status stunStatus;

    [SerializeField] Animator vfx;

    public override void Initialise(CharacterStats caster, Weapon w, bool overrideWpn = false)
    {
        base.Initialise(caster, w, overrideWpn);

        //Freeze Player
        PlayerStats.instance.AddStatus(stunStatus, caster);

        //Spawn FX
        if (vfx)
        {
            GameObject go = Instantiate(vfx.gameObject, transform.position, Quaternion.identity);

            go.GetComponent<Animator>().SetTrigger("Trigger");

            Destroy(go, 2);
        }

        End();
    }
}
