﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Weapon", menuName = "Weapon")]
public class Weapon : Item
{
    public float damage = 15;
    public float attackSpeed = 1;

    [Space]
    public AttackAction attackThing;

    public GameObject mesh;


    [Header("AI")]

    public float idealRange;

    [Space]

    public float retreatRange;

    [Space]

    public float swingOffset;
}
