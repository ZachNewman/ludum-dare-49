﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class CombatController : MonoBehaviour
{
    [SerializeField] Weapon equippedWeapon;
    public Weapon GetHeldWeapon() { return equippedWeapon; }
    [SerializeField] Transform weaponHoldPoint;

    [Space]

    public Transform attackPoint;

    float attacking = 0;
    public bool IsAttacking() { return attacking > 0; }

    CharacterStats statistics;

    bool tryAttack;

    private void Awake()
    {
        statistics = GetComponent<CharacterStats>();
    }

    // Start is called before the first frame update
    void Start()
    {
        EquipWeapon(equippedWeapon);
    }
    private void Update()
    {
        if (statistics.IsDead())
            return;

        if(IsAttacking())
            attacking -= Time.deltaTime;

        if (tryAttack)
            Attack();
    }

    public void EquipWeapon(Weapon w)
    {
        //If no weapon
        if (!w)
            return;

        //If already have object equipped which is in scene
        if (equippedWeapon)
        {
            foreach (Transform child in weaponHoldPoint)
            {
                Destroy(child.gameObject);
            }
        }

        GameObject inst = Instantiate(w.mesh, weaponHoldPoint);

        Collider c = inst.GetComponent<Collider>();
        if (c)
            c.enabled = false;

        equippedWeapon = w;
    }

    //Attack (AI compatible)
    public void Attack()
    {
        //If you are already attacking
        if (IsAttacking())
            return;

        //If you have nothing equipped
        if (equippedWeapon == null)
            return;

        if (statistics.IsDead())
            return;

        //Instantiate the attack action (this can be a projectile, melee collider, etc)
        AttackAction aa = Instantiate(equippedWeapon.attackThing.gameObject, attackPoint).GetComponent<AttackAction>();

        if (aa.spawnOnCaster)
            aa.transform.position = transform.position;

        //Start it
        aa.Initialise(statistics, equippedWeapon);

        //Set our attack cooldown
        attacking = Mathf.Clamp(equippedWeapon.attackSpeed - statistics.GetEffect(StatusEffect.attackSpeed), 0.2f, Mathf.Infinity);

        if(statistics.GetAnimator())
            statistics.GetAnimator().SetTrigger("Attack");
    }

    //Player input to attack
    public void OnAttack(InputAction.CallbackContext value)
    {
        if (value.started)
            tryAttack = true;
        else if (value.canceled)
            tryAttack = false;
        
    }
}
