using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Reflection;

public class Enemy : MonoBehaviour
{
    public int EnemyLevel;
    public EnemyDropList[] EnemyDropsList;
    public LootData[] ExtraLoot;

    [ContextMenu("DropLoot")]
    public void SpawnLoot()
    {
        LootManager.instance.LootHolderInstance = Instantiate(LootManager.instance.LootHolder);

        for (int i = 0; i < EnemyDropsList.Length; i++)
        {
            for (int j = 0; j < EnemyDropsList[i].loots.Length; j++)
            {
                EnemyDropsList[i].loots[j].DropLoot(EnemyLevel);
            }
        }

        for (int i = 0; i < ExtraLoot.Length; i++)
        {
            ExtraLoot[i].DropLoot(EnemyLevel);
        }
    }   
}
