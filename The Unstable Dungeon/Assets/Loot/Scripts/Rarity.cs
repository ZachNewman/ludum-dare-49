using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum RarityType
{
    Common,
    Uncommon,
    Rare
}

public static class Rarity 
{
    public static float CommonChance = 60;
    public static float UncommonChance = 30;
    public static float RareChance = 10;

    public static RarityType ChooseRarity()
    {
        float roll = Random.Range(0, CommonChance + UncommonChance + RareChance);

        if(roll < CommonChance)
        {
            return RarityType.Common;
        }
        else if(roll < CommonChance + UncommonChance)
        {
            return RarityType.Uncommon;
        }
        else
        {
            return RarityType.Rare;
        }
    }
}
