using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class LootData 
{
    public BaseLoot Loot;
    [Range(0, 100)] public float DropChance;
    public int MaxPossibleDrops;

    public void DropLoot(int EnemyLevel)
    {
        Loot.DropLoot(EnemyLevel, DropChance, MaxPossibleDrops);
    }
}
