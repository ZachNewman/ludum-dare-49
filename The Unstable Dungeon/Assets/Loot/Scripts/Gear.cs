using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Loot", menuName = "Loot/Gear", order = 4)]
public class Gear : Loot
{
    public RarityType RarityType;
    public int TierNumber;
    public int Damage;
    public int Armour;
    public Prefix Prefix;
    public Suffix Suffix;
    public int PrefixValue;
    public int SuffixValue;

    public TierStats[] GearStats;
    [System.Serializable]
    public class TierStats
    {
        public Vector2 DamageRange;
        public Vector2 ArmourRange;

        public Prefix[] PossiblePrefixes;

        public Suffix[] PossibleSuffixes;
    }

    public override void PopulateItemInformation(int EnemyLevel)
    {
        RarityType = Rarity.ChooseRarity();
        TierNumber = Tier.ChooseTier(EnemyLevel);
        ChooseStats();
        ChooseAffixes();
    }

    void ChooseStats()
    {
        if(TierNumber > GearStats.Length)
        {
            TierNumber = GearStats.Length;
        }

        Damage = Random.Range((int)GearStats[TierNumber].DamageRange.x, (int)GearStats[TierNumber].DamageRange.y);
        Armour = Random.Range((int)GearStats[TierNumber].ArmourRange.x, (int)GearStats[TierNumber].ArmourRange.y);
    }

    void ChooseAffixes()
    {
        Prefix = null;
        Suffix = null;

        if(RarityType == RarityType.Uncommon)
        {
            int roll = Random.Range(0, 2);

            if(roll == 0)
            {
                roll = Random.Range(0, GearStats[TierNumber].PossiblePrefixes.Length);
                Prefix = GearStats[TierNumber].PossiblePrefixes[roll];
                PrefixValue = Random.Range((int)GearStats[TierNumber].PossiblePrefixes[roll].Range.x, (int)GearStats[TierNumber].PossiblePrefixes[roll].Range.y);
            }
            else
            {
                roll = Random.Range(0, GearStats[TierNumber].PossibleSuffixes.Length);
                Suffix = GearStats[TierNumber].PossibleSuffixes[roll];
                SuffixValue = Random.Range((int)GearStats[TierNumber].PossibleSuffixes[roll].Range.x, (int)GearStats[TierNumber].PossibleSuffixes[roll].Range.y);
            }
        }
        else if (RarityType == RarityType.Rare)
        {
            int roll = Random.Range(0, GearStats[TierNumber].PossiblePrefixes.Length);
            Prefix = GearStats[TierNumber].PossiblePrefixes[roll];
            PrefixValue = Random.Range((int)GearStats[TierNumber].PossiblePrefixes[roll].Range.x, (int)GearStats[TierNumber].PossiblePrefixes[roll].Range.y);

            roll = Random.Range(0, GearStats[TierNumber].PossibleSuffixes.Length);
            Suffix = GearStats[TierNumber].PossibleSuffixes[roll];
            SuffixValue = Random.Range((int)GearStats[TierNumber].PossibleSuffixes[roll].Range.x, (int)GearStats[TierNumber].PossibleSuffixes[roll].Range.y);
        }
    }
}
