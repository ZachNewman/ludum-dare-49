using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Loot", menuName = "Loot/Currency", order = 6)]
public class Currency : Loot
{
    public int Amount;
    
    public override void DropLoot(int EnemyLevel, float DropChance, int MaxPossibleDrops)
    {
        Amount = 0;

        for (int i = 0; i < MaxPossibleDrops; i++)
        {
            if (CompareRoll(DropChance))
            {
                Amount++;
            }
        } 

        InstantiateLoot();
    }
}
