using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public static class Tier 
{
    public static int TierNumber;

    public static int ChooseTier(int EnemyLevel)
    {
        return Random.Range(0, EnemyLevel / 20) + 1;
    }
}
