using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Affix : ScriptableObject
{
    public string Name;
    public string Description;
    public Vector2 Range;
}
