using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseLoot : ScriptableObject
{
    public virtual void DropLoot(int EnemyLevel, float DropChance, int MaxPossibleDrops)
    {

    }

    protected bool CompareRoll(float DropChance)
    {
        float roll = Roll100();

        if (roll < DropChance)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    protected float Roll100()
    {
        return Random.Range(0, 100);
    }
}
