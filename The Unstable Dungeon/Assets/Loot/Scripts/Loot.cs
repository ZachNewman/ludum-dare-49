using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Loot", menuName = "Loot/Loot", order = 3)]
public class Loot : BaseLoot
{
    public override void DropLoot(int EnemyLevel, float DropChance, int MaxPossibleDrops)
    {
        for (int i = 0; i < MaxPossibleDrops; i++)
        {
            if (CompareRoll(DropChance))
            {
                PopulateItemInformation(EnemyLevel);
                InstantiateLoot();
            }
        }
    }

    public virtual void PopulateItemInformation(int EnemyLevel)
    {

    }

    public void InstantiateLoot()
    {
        GameObject Loot = Instantiate(LootManager.instance.Loot, LootManager.instance.LootHolderInstance.transform);
        Loot.GetComponent<LootPrefab>().Loot = Instantiate(this);
    }
}
