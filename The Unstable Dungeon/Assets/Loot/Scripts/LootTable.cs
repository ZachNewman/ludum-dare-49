using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Loot", menuName = "Loot/LootTable", order = 2)]
public class LootTable : BaseLoot
{
    public LootData[] TableOfLoot;

    public override void DropLoot(int EnemyLevel, float DropChance, int MaxPossibleDrops)
    {
        for (int i = 0; i < MaxPossibleDrops; i++)
        {
            for (int j = 0; j < TableOfLoot.Length; j++)
            {
                TableOfLoot[j].DropLoot(EnemyLevel);
            }
        }
    }
}
