using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Loot", menuName = "Loot/EnemyDropList", order = 1)]
public class EnemyDropList : ScriptableObject
{
    public LootData[] loots;
}
